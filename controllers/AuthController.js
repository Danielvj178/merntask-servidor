const User = require("../models/User")
const bcryptjs = require('bcryptjs');
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

exports.authUser = async (req, res) => {
    // Revisar si hay errores al consumir la API
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() })
    }

    // Extraer la info enviada desde la API
    const { email, password } = req.body;

    try {
        // Revisar en la BD que el usuario exista
        let user = await User.findOne({ email });
        //let msgError = res.status(400).json({ msg: 'El usuario y/o password es incorrecto' });
        //let msgSuccess = res.status(200);

        if (!user) {
            return res.status(400).json({ msg: 'El usuario y/o password es incorrecto' });;
        } else {
            const correctPass = await bcryptjs.compare(password, user.password);

            if (!correctPass)
                return res.status(400).json({ msg: 'El usuario y/o password es incorrecto' });
            else {
                // Crear y firmar jwt
                const payload = {
                    user: {
                        id: user.id
                    }
                };

                // Firmar el JWT
                jwt.sign(payload, process.env.SECRET_KEY, {
                    // Tiempo de duración del token
                    expiresIn: 36000
                }, (error, token) => {
                    if (error) throw error;

                    // Mensaje de guardado correcto
                    res.json({ token, prueba: 'Ok' });
                });
            }

        }
    } catch (error) {
        console.log(error);
    }
}

exports.userAuthenticate = async (req, res) => {
    try {
        // El menos dentro de select es para que no traiga ese campo
        const user = await User.findById(req.user.id).select('-password');
        res.json({ user });
    } catch (error) {
        res.status(500).json({ msg: 'Hubo un error' });
    }
}