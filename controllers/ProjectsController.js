const Project = require('../models/Project');
const { validationResult } = require('express-validator');

// Crear proyectos
exports.createProject = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        const project = new Project(req.body);

        // Se obtiene del middleware (auth)
        project.creator = req.user.id;
        project.save();
        res.json(project);
    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: 'Problema con el servidor' });
    }
}

// Obtener todos los proyectos del usuario actual
exports.getProjects = async (req, res) => {
    try {
        const projects = await Project.find({ creator: req.user.id }).sort({ creator: -1 });
        res.json(projects);


    } catch (error) {
        console.log(error);
        return res.status(500).send('Hubo un error');
    }
}

// Actualizar proyecto
exports.updateProject = async (req, res) => {

    // Revisar si hay errores
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() })
    }

    // Se extrae la información
    const { name } = req.body;
    const editProject = {};

    if (name) {
        editProject.name = name;
    }

    try {
        // Revisar el ID
        let project = await Project.findById(req.params.id);

        // Verificar si el proyecto existe
        if (!project) {
            return res.status(404).json({ msg: 'Proyecto no encontrado' });
        }

        // Verificar si el creador del proyecto es el mismo
        if (project.creator.toString() !== req.user.id) {
            return res.status(401).json({ msg: 'No Autorizado' });
        }

        // Actualizar el proyecto
        project = await Project.findByIdAndUpdate({ _id: req.params.id }, { $set: editProject }, { new: true });

        return res.json({ project });

    } catch (error) {
        console.log(error);
        return res.status(500).send('Error en el servidor');
    }
}

// Borrar proyecto
exports.deleteProject = async (req, res) => {
    try {
        // Revisar el ID
        let project = await Project.findById(req.params.id);

        // Verificar si existe el proyecto
        if (!project) {
            return res.status(404).json({ msg: 'Proyecto no encontrado' });
        }

        // Verificar que el creador del proyecto sea el mismo
        if (project.creator.toString() !== req.user.id) {
            return res.status(401).json({ msg: 'No autorizado' });
        }

        // Borrar proyecto
        await Project.findByIdAndDelete({ _id: req.params.id });

        res.json({ project });

    } catch (error) {
        console.log(error);
        return res.status(500).send('Error en el servidor');
    }
}