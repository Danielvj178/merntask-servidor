const Task = require('../models/Task');
const Project = require('../models/Project');
const { validationResult } = require('express-validator');

exports.createTask = async (req, res) => {
    try {
        // Validar que llegue el cuerpo de la solicitud correctamente
        validateError(req, res);

        const { project } = req.body;

        // Revisar que venga el proyecto
        const projectSearch = await Project.findById(project);

        if (!projectSearch) {
            return res.status(404).json({ msg: 'Proyecto no encontrado' });
        }

        // Validar que el usuario sea el dueño del proyecto
        if (projectSearch.creator.toString() !== req.user.id) {
            return res.status(401).json({ msg: 'No tiene autorización' });
        }

        const task = new Task(req.body);
        await task.save();
        res.json(task);
    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: 'Error en servidor' });
    }
}

// Obtenemos las tareas por proyecto
exports.getTask = async (req, res) => {
    try {
        // Validar que llegue el cuerpo de la solicitud correctamente
        validateError(req, res);
        // query es cuando desde el frontend se envía el atributo dentro de un arreglo con la palabra params
        const { project } = req.query;

        // Revisar que venga el proyecto
        const projectSearch = await Project.findById(project);

        if (!projectSearch) {
            return res.status(404).json({ msg: 'Proyecto no encontrado' });
        }

        // Validar que el usuario sea el dueño del proyecto
        if (projectSearch.creator.toString() !== req.user.id) {
            return res.status(401).json({ msg: 'No tiene autorización' });
        }

        // Obtener las tareas por proyecto
        const tasks = await Task.find({ project });
        res.json({ tasks })

    } catch (error) {
        console.log(error);
        res.status(500).send('Hubo un error');
    }
}

// Actualizar tarea
exports.updateTask = async (req, res) => {
    try {
        // Validar que llegue el cuerpo de la solicitud correctamente
        validateError(req, res);

        // Verificar si la tarea existe
        let task = await Task.findById(req.params.id);

        if (!task) {
            return res.status(404).json({ msg: 'Tarea no encontrada' });
        }

        // Extraer parametros del cuerpo
        const { project, name, status } = req.body;

        // Revisar que venga el proyecto
        const projectSearch = await Project.findById(project);

        // Validar que el usuario sea el dueño del proyecto
        if (projectSearch.creator.toString() !== req.user.id) {
            return res.status(401).json({ msg: 'No tiene autorización' });
        }

        // Crear un objeto con la nueva información 
        const newTask = {};
        newTask.name = name;
        newTask.status = status;

        //Guardar tarea
        task = await Task.findOneAndUpdate({ _id: req.params.id }, newTask, { new: true });

        res.json({ task });

    } catch (error) {
        console.log(error);
        res.status(500).send('Hubo un error');
    }
}

//Borrar Tarea
exports.deleteTask = async (req, res) => {
    try {
        validateError(req, res);

        // Extraer información del cuerpo de la petición
        const { project } = req.query;

        // Buscar el proyecto para validar permisos
        const projectSearch = await Project.findById(project);

        // Encontrar tarea
        const task = await Task.findById(req.params.id);

        // Verificar que exista la tarea
        if (!task) {
            return res.status(404).json({ msg: 'Tarea no encontrada' });
        }

        // Verificar autorización del proyecto
        if (projectSearch.creator.toString() !== req.user.id) {
            return res.status(401).json({ msg: 'No tiene autorización' });
        }

        // Borrar proyecto
        await Task.findByIdAndDelete({ _id: req.params.id });

        return res.json({ msg: 'Tarea Eliminada' });
    } catch (error) {
        console.log(error);
        return res.status(500).json({ msg: 'Hubo un error en el servidor' });
    }
}

const validateError = (req, res) => {
    // Validar que llegue el cuerpo de la solicitud correctamente
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() })
    }
}