const User = require("../models/User")
const bcryptjs = require('bcryptjs');
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

exports.createUser = async (req, res) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() })
    }

    try {
        // Destructuring del Json que se envía
        const { email, password } = req.body;

        let user = await User.findOne({ email });

        if (user) {
            res.status(400).json({ msg: 'Hubo un error al crear el usuario, intente nuevamente' });
        } else {
            // Crea el nuevo usuario con la estructura definida en el modelo
            user = new User(req.body);

            // Hashear el password
            // Cuantas veces se realizará el Hash el password
            const salt = await bcryptjs.genSalt(10);
            user.password = await bcryptjs.hash(password, salt);

            // Guarda el usuario en la BD
            await user.save();

            // Crear y firmar jwt
            const payload = {
                user: {
                    id: user.id
                }
            };

            // Firmar el JWT
            jwt.sign(payload, process.env.SECRET_KEY, {
                // Tiempo de duración del token
                expiresIn: 36000
            }, (error, token) => {
                if (error) throw error;

                // Mensaje de guardado correcto
                res.json({ token });
            });

        }
    } catch (error) {
        console.log(error);
        res.status(400).send('Hubo un error');
    }
}