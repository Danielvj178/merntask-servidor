const express = require('express');
const connectDB = require('./config/db');
const cors = require('cors');

//Crear el servidor
const app = express();

// Conectar a la BD
connectDB();

// Habilitar cors
//app.use(cors());

app.use(cors({ credentials: true, origin: true }));
app.options("*", cors());

// Habilitar express.json
// Permite recibir un json y tratarlo en el controlador
app.use(express.json({ extended: true }));

// Puerto de a APP
const port = process.env.PORT || 4000;
app.use('/api/user', require('./routes/user'));
app.use('/api/auth', require('./routes/auth'));
app.use('/api/projects', require('./routes/projects'));
app.use('/api/tasks', require('./routes/tasks'));

// Arrancar la app
app.listen(port, '0.0.0.0', () => {
    console.log(`El servidor está funcionando en el puerto ${port}`);
});