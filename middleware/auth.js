const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
    // Leer el token del Header
    const token = req.header('x-auth-token');

    // Revisar si no hay token
    if (!token) {
        return res.status(401).json({ msg: 'Token no enviado' })
    }

    // Validar el token 
    try {
        // Esta función se encarga de verificar que el token generado sea correcto
        const encryption = jwt.verify(token, process.env.SECRET_KEY);
        // Se obtiene el usuario porque en el UserController en el payload se carga el user
        req.user = encryption.user;
        // Envía al siguiente Middleware
        next();
    } catch (error) {
        console.log(error);
        return res.status(401).json({ msg: 'Token no válido' });
    }
}