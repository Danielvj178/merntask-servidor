const mongoose = require('mongoose');

const ProjectSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    creator: {
        // crear una relación de otra tabla, en este caso se relaciona por el ID
        type: mongoose.Schema.Types.ObjectId,
        // Debe ir el nombre dado al modelo en el exports
        ref: 'User'
    },
    create: {
        type: Date,
        default: Date.now()
    }
});

module.exports = mongoose.model('Project', ProjectSchema);