// Rutas para autenticar usuarios
const express = require('express');
const router = express.Router();
const { check } = require('express-validator');
const AuthController = require('../controllers/AuthController');
const auth = require('../middleware/auth');

// Autentica un usuario
// api/auth
router.post('/',
    [
        check('email', 'Agrega un email válido').isEmail(),
        check('password', 'El password debe ser mínimo de 6 caracteres').isLength({ min: 6 })
    ],
    AuthController.authUser
)

// Obtiene el usuario autenticado
router.get('/',
    auth,
    AuthController.userAuthenticate
)

module.exports = router;