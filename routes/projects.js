const express = require('express');
const router = express.Router();
const { check } = require('express-validator');
const ProjectsController = require('../controllers/ProjectsController');
const auth = require('../middleware/auth');

// Crea un proyecto
// api/projects
// Se ordenan los Middleware que se quieran ejecutar
router.post('/',
    auth,
    [
        check('name', 'El nombre es obligatorio').not().isEmpty()
    ],
    ProjectsController.createProject
)

// Obtener todos los proyectos
// api/projects
router.get('/',
    auth,
    ProjectsController.getProjects
)

// Actualizar un proyecto
// api/projects/id
router.put('/:id',
    auth,
    [
        check('name', 'El nombre es obligatorio').not().isEmpty()
    ],
    ProjectsController.updateProject
);

// Eliminar proyecto
// api/projects/id
router.delete('/:id',
    auth,
    ProjectsController.deleteProject
);

module.exports = router;