// Rutas para tareas
const express = require('express');
const router = express.Router();
const TasksController = require('../controllers/TasksController');
const auth = require('../middleware/auth');
const { check } = require('express-validator');

// Crear una tarea
// api/tasks
router.post('/',
    auth,
    [
        check('name', 'El nombre es obligatorio').not().isEmpty(),
        check('project', 'El proyecto es obligatorio').not().isEmpty()
    ],
    TasksController.createTask
);

// Obtener las tareas por proyecto
// api/tasks
router.get('/',
    auth,
    TasksController.getTask
);

// Actualizar tareas
// api/tasks/id - UPDATE
router.put('/:id',
    auth,
    [
        check('project', 'El proyecto es obligatorio').not().isEmpty()
    ],
    TasksController.updateTask
);

// Eliminar Tareas
// api/tasks/id - DELETE
router.delete('/:id',
    auth,
    TasksController.deleteTask
)

module.exports = router