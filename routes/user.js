// Rutas para crear usuarios
const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController');
const { check } = require('express-validator');

// Crea un usuario
// api/usuarios
router.post('/',
    [
        check('name', 'El nombre es obligatorio').not().isEmpty(),
        check('email', 'Agrega un email válido').isEmail(),
        check('password', 'El password debe ser mínimo de 6 caracteres').isLength({ min: 6 })
    ],
    UserController.createUser
);

module.exports = router;